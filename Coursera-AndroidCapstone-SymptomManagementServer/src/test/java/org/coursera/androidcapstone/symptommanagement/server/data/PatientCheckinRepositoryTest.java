package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class PatientCheckinRepositoryTest {

	@Inject
	private PatientCheckinRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindByPatientIdAndCheckinId() {
		
		long patientId = 1L;
		long checkinId = 1L;
		PatientCheckin checkin = repository.findByPatientIdAndCheckinId(patientId, checkinId);
		assertNotNull(checkin);
		assertEquals("SEV", checkin.getMouthThroatPain());
		assertEquals("S", checkin.getDoesStopEatDrink());
		System.out.println("checkin date/time: " + checkin.getCheckinDateTime());
		
		
	}

	@Test
	public void testPatientCheckinInsert() throws Exception {
		long patientId = 3L;
		String doesStopEatDrink = PainStopEatDrinkQuestionResponse.SOME.getCode();
		
		PatientCheckin checkin = new PatientCheckin();
		checkin.setPatientId(patientId);
		checkin.setDoesStopEatDrink(doesStopEatDrink);
		checkin.setMouthThroatPain(PainSeverityLevel.MODERATE.getCode());
		Date date = ApplicationUtils.convertStringToSqlDate("11/01/2014"); 
		checkin.setCheckinDateTime(new Timestamp(date.getTime()));
		
		PatientCheckin newCheckin = repository.save(checkin);
		
		assertNotNull(newCheckin);
		assertEquals(patientId, newCheckin.getPatientId());
		assertEquals(doesStopEatDrink, newCheckin.getDoesStopEatDrink());
		//make sure the insert was done by checking returned checkinId
		assertTrue(newCheckin.getCheckinId() != 0L);
		System.out.println("Inserted checkin: " + newCheckin);
	}
	
	@Test
	public void testFindCheckinsByPatientId() throws Exception {
		long patientId = 3L;
		
		List<PatientCheckin> checkins = repository.findCheckinsByPatientId(patientId);
		
		assertNotNull(checkins);
		assertFalse(checkins.isEmpty());
		assertTrue(checkins.size() == 2);
	}
	
	@Test
	public void testFindCheckinsByPatientId_CheckProperOrder() throws Exception {
		long patientId = 3L;
		
		List<PatientCheckin> checkins = repository.findCheckinsByPatientId(patientId);
		
		assertNotNull(checkins);
		assertFalse(checkins.isEmpty());
		assertTrue(checkins.size() == 2);
		assertEquals(2L, checkins.get(0).getCheckinId());
		assertEquals(3L, checkins.get(1).getCheckinId());
		//assert that the first checkin is after the second
		assertTrue(checkins.get(0).getCheckinDateTime().after(checkins.get(1).getCheckinDateTime()));
	}
	
}
