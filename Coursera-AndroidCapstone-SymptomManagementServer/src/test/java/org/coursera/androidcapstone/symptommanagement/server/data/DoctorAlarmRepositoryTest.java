package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlertCriteria;
import org.coursera.androidcapstone.symptommanagement.server.TestConfig;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class DoctorAlarmRepositoryTest {

	@Inject
	private DoctorAlarmRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInsert() {
		String alertCode = DoctorAlertCriteria.CANT_EAT_12.getCode();
		char hasDoctorBeenNotified = 'N';
		long patientId = 3L;
		long checkinId = 1L;
		
		DoctorAlarm alarm = new DoctorAlarm();
		alarm.setAlertCode(alertCode );
		alarm.setHasDoctorBeenNotified(hasDoctorBeenNotified);
		alarm.setPatientId(patientId);
		alarm.setCheckinId(checkinId);

		DoctorAlarm newAlarm = repository.save(alarm);
		
		assertTrue(newAlarm.getAlertId() != 0);
		assertEquals(alertCode, newAlarm.getAlertCode());
		assertEquals(hasDoctorBeenNotified, newAlarm.getHasDoctorBeenNotified());
		assertEquals(patientId, newAlarm.getPatientId());
	}

	@Test
	public void testFindOne() {
		
		long alertId = 1L;
		String alertCode = DoctorAlertCriteria.SEVERE_PAIN_12.getCode();
		char hasDoctorBeenNotified = 'N';
		long patientId = 1L;
		
		
		
		DoctorAlarm alarm = repository.findOne(alertId);
		
		assertEquals(alertId, alarm.getAlertId());
		assertEquals(alertCode, alarm.getAlertCode());
		assertEquals(hasDoctorBeenNotified, alarm.getHasDoctorBeenNotified());
		assertEquals(patientId, alarm.getPatientId());
	}

	
	@Test
	public void testUpdateHasBeenNotofied() throws Exception {
		
		long alertId = 1L;
		String alertCode = DoctorAlertCriteria.CANT_EAT_12.getCode();
		char hasDoctorBeenNotified = 'Y';//is 'N' in database
		long patientId = 1L;
		
		DoctorAlarm alarm = new DoctorAlarm();
		alarm.setAlertId(alertId);
		alarm.setAlertCode(alertCode);
		alarm.setHasDoctorBeenNotified(hasDoctorBeenNotified);
		alarm.setPatientId(patientId);

		DoctorAlarm newAlarm = repository.save(alarm);
		
		assertTrue(newAlarm.getAlertId() == 1L);
		assertEquals(alertCode, newAlarm.getAlertCode());
		assertEquals(hasDoctorBeenNotified, newAlarm.getHasDoctorBeenNotified());
		assertEquals(patientId, newAlarm.getPatientId());
		
		//Make sure hasBeenNotified is 'Y' by querying that record
		DoctorAlarm alarm2 = repository.findOne(alertId);
		assertEquals(hasDoctorBeenNotified, alarm2.getHasDoctorBeenNotified());
	}


	@Test
	public void testFindOne_BogusAlertId_ReturnsNull() {
		
		long alertId = -99L;
		
		DoctorAlarm alarm = repository.findOne(alertId);
		
		assertNull(alarm);
	}
	
	@Test
	public void testFindPatientAlarms() throws Exception {
		
		long doctorId = 2L;//a doctor id
		List<DoctorAlarm> alarms = repository.findPatientAlarms(doctorId);
		
		assertNotNull(alarms);
		assertFalse(alarms.isEmpty());
		System.out.println("Alarms: " + alarms);
		
	}
	
	
	@Test
	public void testFindByCheckinId() throws Exception {
		long checkinId = 1;
		
		DoctorAlarm alarm = repository.findByCheckinId(checkinId);
		
		assertNotNull(alarm);
		assertEquals(1L, alarm.getAlertId());
		assertEquals(1L, alarm.getPatientId());
		assertEquals(DoctorAlertCriteria.SEVERE_PAIN_12.getCode(), alarm.getAlertCode());
		
	}



	@Test
	public void testFindByCheckinId_NotFound() throws Exception {
		long checkinId = -1;
		
		DoctorAlarm alarm = repository.findByCheckinId(checkinId);
		
		assertNull(alarm);
		
	}
}
