package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class MedicationRepositoryTest {

	@Inject
	private MedicationRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindOne() {
		long medicationId = 1L;
		
		Medication med = repository.findOne(medicationId);
		
		assertEquals(medicationId, med.getMedicationId());
		assertEquals("LORTAB", med.getMedicationName());
		
		System.out.println("Med found: " + med);
	}

	@Test
	public void testFindAll() {
		
		Iterable<Medication> meds = repository.findAll();
		
		assertNotNull(meds);
		
		System.out.println("Meds: " +  meds);
		System.out.println("Meds type: " +  meds.getClass().getTypeName());
		assertTrue(meds instanceof List);
		List<Medication> medList = (List<Medication>)meds;
		assertTrue(medList.size() == 3);
	}
	
	
	@Test
	public void testFindMedicationsByPatientId() throws Exception {
		
		long patientId = 1L;
		
		List<Medication> meds = repository.findMedicationsByPatient(patientId);
		
		assertNotNull(meds);
		assertFalse(meds.isEmpty());
		assertTrue(meds.size() == 2);
		assertEquals(1L, meds.get(0).getMedicationId());
		assertEquals(3L, meds.get(1).getMedicationId());
		
	}

}
