package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class DoctorPatientRepositoryTest {

	@Inject
	private DoctorPatientRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindAllPatients_Found() {
		long doctorId = 2L;
		
		List<Patient> patients = repository.findAllPatients(doctorId);

		assertNotNull(patients);
		assertFalse(patients.isEmpty());
		assertTrue(patients.size() == 2);
		System.out.println("Patients: " + patients);
		for (Patient patient : patients) {
			System.out.println(patient.getClass().getTypeName());
			System.out.println(patient);
		}
	}
	

	@Test
	public void testFindPatientByName_Found() throws Exception {
		
		long doctorId = 2L;
		String patientFirstName = "Patient";
		String patientLastName = "Number1";
		
		Patient patient = repository.findPatientByName(doctorId, patientFirstName, patientLastName);
		
		assertNotNull(patient);
		assertEquals(patientFirstName, patient.getFirstName());
		assertEquals(patientLastName, patient.getLastName());
	}


	@Test
	public void testFindPatientByName_UpperCaseName_Found() throws Exception {
		
		long doctorId = 2L;
		String patientFirstName = "Patient";
		String patientLastName = "Number1";
		
		Patient patient = repository.findPatientByName(doctorId, patientFirstName.toUpperCase(), patientLastName.toUpperCase());
		
		assertNotNull(patient);
		assertEquals(patientFirstName, patient.getFirstName());
		assertEquals(patientLastName, patient.getLastName());
	}
	
	@Test
	public void testFindPatientByName_NotFound() throws Exception {
		
		long doctorId = 2L;
		String patientFirstName = "Foobar";
		String patientLastName = "Number1";
		
		Patient patient = repository.findPatientByName(doctorId, patientFirstName, patientLastName);
		
		assertNull(patient);
	}
}
