package org.coursera.androidcapstone.symptommanagement.server.data;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestPersistenceConfig.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class PatientAlertCountRepositoryTest {

	@Inject
	private PatientAlertCountRepository repository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindPatientActiveAlertCounts() {
		List<PatientAlertCount> alertCounts = repository.findPatientActiveAlertCounts();
		System.out.println("Active alerts: " + alertCounts);
		assertTrue(alertCounts.size() == 1);
	}

	@Test
	public void testFindPatientAllAlertCounts() {
		List<PatientAlertCount> alertCounts = repository.findPatientAllAlertCounts();
		System.out.println("All alerts: " + alertCounts);
		assertTrue(alertCounts.size() == 2);
	}


	@Test
	public void testFindPatientActiveAlertCountsByPatientId() {
		long patientId = 1;
		PatientAlertCount alertCount = repository.findPatientActiveAlertCountsByPatientId(patientId);
		assertTrue(alertCount.getAlertCount() == 1);
	}

	@Test
	public void testFindPatientAllAlertCountsByPatientId() {
		long patientId = 1;
		PatientAlertCount alertCount = repository.findPatientAllAlertCountsByPatientId(patientId);
		assertTrue(alertCount.getAlertCount() == 1);
	}


	@Test
	public void testFindPatientAllAlertCountsByPatientId_NotFound() {
		long patientId = 99;
		PatientAlertCount alertCount = repository.findPatientAllAlertCountsByPatientId(patientId);
		assertNull(alertCount);
	}
}
