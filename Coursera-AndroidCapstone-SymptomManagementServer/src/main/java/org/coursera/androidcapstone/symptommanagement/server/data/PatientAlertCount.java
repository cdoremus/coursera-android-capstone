package org.coursera.androidcapstone.symptommanagement.server.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="patient_alert", schema="app")
public class PatientAlertCount implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="patient_id")
	private long patientId;
	@Column(name="alert_count")
	private int alertCount;
	
	public PatientAlertCount(){
	}
	
	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public int getAlertCount() {
		return alertCount;
	}
	public void setAlertCount(int activeAlertCount) {
		this.alertCount = activeAlertCount;
	}

	@Override
	public String toString() {
		return "PatientAlertCount [patientId=" + patientId + ", alertCount="
				+ alertCount + "]";
	}
	
}
