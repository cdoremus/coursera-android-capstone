package org.coursera.androidcapstone.symptommanagement.server.data;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlertCriteria;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;


/**
 * 
 * Manages alerts
 * 
 * A doctor is alerted if a patient
 * <ul> 
 * <li>Experiences 12 hours of �severe pain,�</li>
 * <li>16 or more hours of �moderate� or �severe pain�</li>
 * <li>12 hours of �I can�t eat.�</li>
 * <ul> 
 *
 */
public class AlertManager {

	@Inject
	private DoctorAlarmRepository repository;
	
	
	/**
	 * 
	 * @param checkins
	 * @return
	 */
	public DoctorAlarm saveAlarmForLatestCheckin(List<PatientCheckin> checkins) {
		DoctorAlarm alarm = new DoctorAlarm();
		DoctorAlertCriteria alertCriteria = null; 
		PatientCheckin alertableCheckin = null;
		PatientCheckin checkin1 = checkinWith12HoursSeverePain(checkins);
		PatientCheckin checkin2 = checkinWithAtLeast16HoursOrMoreModerateOrSeverePain(checkins);
		PatientCheckin checkin3 = checkinWith12HoursICannotEat(checkins);
		if (checkin1 != null) {
			alertableCheckin = checkin1;
			alertCriteria = DoctorAlertCriteria.SEVERE_PAIN_12;
			
		} else if (checkin2 != null) {
			alertableCheckin = checkin2;
			alertCriteria = DoctorAlertCriteria.MODERATE_TO_SEVERE_PAIN_16;
			
		} else if (checkin3 != null) {
			alertableCheckin = checkin3;
			alertCriteria = DoctorAlertCriteria.CANT_EAT_12;
		}

		if (alertableCheckin != null) {
			alarm.setAlertCode(alertCriteria.getCode());
			alarm.setCheckinId(alertableCheckin.getCheckinId());
			alarm.setPatientId(alertableCheckin.getPatientId());
			alarm.setHasDoctorBeenNotified('N');
			repository.save(alarm);
		}
		return alarm;
	}
	
	
	
	public PatientCheckin checkinWith12HoursSeverePain(List<PatientCheckin> checkins) {
		PatientCheckin alertableCheckin = null;
		int indexMax = checkins.size();
		PatientCheckin checkin1 = checkins.get(0);
		for(int i=1; i < indexMax; i++) {
			PatientCheckin checkin2 = checkins.get(i);
			Timestamp checkin1Ts = checkin1.getCheckinDateTime();
			Timestamp checkin2Ts = checkin2.getCheckinDateTime();
			PainSeverityLevel painLevel1 = PainSeverityLevel.findSeverityLevelByCode(checkin1.getMouthThroatPain()); 
			PainSeverityLevel painLevel2 = PainSeverityLevel.findSeverityLevelByCode(checkin2.getMouthThroatPain()); 
			boolean isDiffGreaterThan12hrs = ApplicationUtils.isDifferenceGreaterThanHrs(checkin1Ts, checkin2Ts, 12);
			if (isDiffGreaterThan12hrs 
					&& (painLevel1 == PainSeverityLevel.SEVERE) 
					&& (painLevel2 == PainSeverityLevel.SEVERE) ){
				alertableCheckin = checkin1;
				break;
			}
		}
		return alertableCheckin;
	}
	
	public PatientCheckin  checkinWithAtLeast16HoursOrMoreModerateOrSeverePain(List<PatientCheckin> checkins) {
		PatientCheckin alertableCheckin = null;
		int indexMax = checkins.size();
		PatientCheckin checkin1 = checkins.get(0);
		for(int i=1; i < indexMax; i++) {
			PatientCheckin checkin2 = checkins.get(i);
			Timestamp checkin1Ts = checkin1.getCheckinDateTime();
			Timestamp checkin2Ts = checkin2.getCheckinDateTime();
			PainSeverityLevel painLevel1 = PainSeverityLevel.findSeverityLevelByCode(checkin1.getMouthThroatPain()); 
			PainSeverityLevel painLevel2 = PainSeverityLevel.findSeverityLevelByCode(checkin2.getMouthThroatPain()); 
			boolean isDiffGreaterThan16hrs = ApplicationUtils.isDifferenceGreaterThanHrs(checkin1Ts, checkin2Ts, 16);
			if (isDiffGreaterThan16hrs 
					&& (painLevel1 == PainSeverityLevel.MODERATE || painLevel1 == PainSeverityLevel.SEVERE) 
					&& (painLevel2 == PainSeverityLevel.MODERATE || painLevel2 == PainSeverityLevel.SEVERE) ){
				alertableCheckin = checkin1;
				break;
			}
		}
		return alertableCheckin;
	}


	public PatientCheckin checkinWith12HoursICannotEat(List<PatientCheckin> checkins) {
		PatientCheckin alertableCheckin = null;
		int indexMax = checkins.size();
		PatientCheckin checkin1 = checkins.get(0);
		for(int i=1; i < indexMax; i++) {
			PatientCheckin checkin2 = checkins.get(i);
			Timestamp checkin1Ts = checkin1.getCheckinDateTime();
			Timestamp checkin2Ts = checkin2.getCheckinDateTime();
			PainStopEatDrinkQuestionResponse painStopEatDrink1 = PainStopEatDrinkQuestionResponse.findQuestonResponseByCode(checkin1.getDoesStopEatDrink()); 
			PainStopEatDrinkQuestionResponse painStopEatDrink2 = PainStopEatDrinkQuestionResponse.findQuestonResponseByCode(checkin2.getDoesStopEatDrink()); 
			boolean isDiffGreaterThan12hrs = ApplicationUtils.isDifferenceGreaterThanHrs(checkin1Ts, checkin2Ts, 12);
			if (isDiffGreaterThan12hrs 
					&& (painStopEatDrink1 == PainStopEatDrinkQuestionResponse.CANT_EAT)
					&& (painStopEatDrink2 == PainStopEatDrinkQuestionResponse.CANT_EAT)) {
				alertableCheckin = checkin1;
				break;				
			}
		}
		return alertableCheckin;
	}
	
	public void saveAlert(DoctorAlarm alert) {
		
		repository.save(alert);
		
	}
}
