package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.coursera.androidcapstone.symptommanagement.common.domain.User;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;

public class AuthorizedUser {// implements UserDetails {

	private static final long serialVersionUID = 1L;

	private User user;
	
	public AuthorizedUser(User user) {
		this.user = user;
	}
	
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		List<UserRoles> roles = new ArrayList<UserRoles>();
//		roles.add(UserRoles.findUserRoleByUserType(user.getUserType()));
//		return roles;
//	}

	public String getPassword() {
		return user.getLoginPassword();
	}

	public String getUsername() {
		return user.getLoginName();
	}

	public boolean isAccountNonExpired() {
		return false;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}


}
