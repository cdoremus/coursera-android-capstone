package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.List;

import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long> {

	@Query(value="select app.medication.medication_id, app.medication.medication_name from app.medication inner join app.patient_medication on app.medication.medication_id=app.patient_medication.medication_id where is_active='Y' and patient_id=?1", nativeQuery=true)
	public List<Medication> findMedicationsByPatient(long patientId);

}
