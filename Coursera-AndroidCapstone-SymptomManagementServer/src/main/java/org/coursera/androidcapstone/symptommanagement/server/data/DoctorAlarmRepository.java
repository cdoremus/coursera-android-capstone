package org.coursera.androidcapstone.symptommanagement.server.data;

import java.util.List;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface DoctorAlarmRepository extends CrudRepository<DoctorAlarm, Long>{

	/**
	 *
	 * Finds alarms by doctor id.
	 * 
	 * TODO: Use thus outer join query to pick up alerts 
	 * select ci.checkin_id, ci.patient_id, ci.checkin_datetime, cq.mouth_throat_pain, cq.does_stop_eat_drink, al.alert_id from app.checkin as ci inner join app.checkin_questions cq on ci.checkin_id=cq.checkin_id left outer join app.patient_alert al on ci.checkin_id=al.checkin_id where ci.patient_id=?1;
	 * 
	 * @param doctorId
	 * @return
	 */
	@Query(value="SELECT ALERT_ID, app.patient_alert.PATIENT_ID, app.user_details.user_firstname, app.user_details.user_lastname ,CHECKIN_ID,ALERT_CODE,DR_NOTIFIED FROM APP.PATIENT_ALERT inner join app.doctor_patient_relation on app.patient_alert.patient_id=app.doctor_patient_relation.patient_id inner join app.user_details on app.patient_alert.patient_id=app.user_details.user_id  where DR_NOTIFIED='N' and app.doctor_patient_relation.doctor_id=?1", nativeQuery=true)
	public List<DoctorAlarm> findPatientAlarms(long doctorId); 
	
	public DoctorAlarm findByCheckinId(long checkinId);

		
}
