Android Capstone Project Readme
===============================

This repository holds the source code for my capstone project for the Mobile Cloud Computing with Android Specialization at Coursera given by professors at the University of Maryland and Vanderbilt University.

The record of my Coursera specialization certificate can be found at [https://www.coursera.org/account/accomplishments/specialization/643TRL5XJ6WY](https://www.coursera.org/account/accomplishments/specialization/643TRL5XJ6WY). This specialization involved taking three other courses with a grade of 90% or above before a student was allowed to do the capstone project. I received a perfect score on the capstone project.

My capstone project was an application that would be used by head-cancer doctors and their patients. The Android app would alert patients to take their medications during the day in a user-configured schedule. They would then use the app to report any symptoms or medication side effects to their doctor. Physicians would review patient-data using the physician' screens in the Android app. Their Android phone would also alert the doctor if certain thresholds were achieved in the strength and duration of patient's symptoms.

A server-side application was created to persisted physician and patient data into a back-end database via a web service using Spring REST controllers and JPA. The back-end database was also used to control authentication and access control on the Android client. 

A short video demonstrating the Android application and the Spring web services and JPA back-end can be found at [https://www.youtube.com/watch?v=Fg_RDpdAhq4](https://www.youtube.com/watch?v=Fg_RDpdAhq4)

Folders in this repository are as follows:

* Coursera-AndroidCapstone-SymptomManagementClient  containing Android app code
* Coursera-AndroidCapstone-SymptomManagementClientTest  containing Android app automated tests
* Coursera-AndroidCapstone-SymptomManagementCommon containing Java code shared by the server application and Android client
* Coursera-AndroidCapstone-SymptomManagementServer containing server-side Java code with unit tests
* Coursera-AndroidCapstone-SymptomManagementUml containing UML diagrams in ObjectAid format

Setting up the database is best done as follows:

* Install a local Derby database from [https://db.apache.org/derby/](Apache Derby web site)
* Set environmental variable DERBY_HOME to the directory where Derby is installed and add $DERBY_HOME/bin to the $PATH
* Set envrionmental variable DERBY_OPTS=-Dderby.system.home=$DERBY_HOME
* Unzip symptommanagement.zip file in the Server project inside the local Derby install directory ($DERBY_HOME).
* Use startNetworkServer (or startNetworkServer.bat) to start the Derby server
* Setup a connection to monitor the database inside a tool like SquirrelSql with the following parameters
    JDBC URL: jdbc:derby://localhost:1527/symptommgmnt (Note that symptommgmnt is the datbase name)
    Username: symptommgmnt
    Password: password1
* Use stopNetworkServer (or stopNetworkServer.bat) when you want to stop the Derby server

The provided database contains data used by the Server project's tests.