package org.coursera.androidcapstone.symptommanagement.common.domain;

import static org.junit.Assert.*;

import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PainSeverityLevelTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToString() {
		assertEquals(PainSeverityLevel.MODERATE.getDisplayName(), PainSeverityLevel.MODERATE.toString());
	}

	@Test
	public void testFindSeverityLevelByCode_Ok() {
		assertEquals(PainSeverityLevel.WELL_CONTROLLED, PainSeverityLevel.findSeverityLevelByCode("WLC"));
		assertEquals(PainSeverityLevel.WELL_CONTROLLED, PainSeverityLevel.findSeverityLevelByCode("wlc"));
		assertEquals(PainSeverityLevel.MODERATE, PainSeverityLevel.findSeverityLevelByCode("MOD"));
		assertEquals(PainSeverityLevel.SEVERE, PainSeverityLevel.findSeverityLevelByCode("SEV"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindSeverityLevelByCode_BogusValue_ThrowsIllegalArgumentException() {
		assertEquals(PainSeverityLevel.WELL_CONTROLLED, PainSeverityLevel.findSeverityLevelByCode("bogus"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFindSeverityLevelByCode_NullValue_ThrowsIllegalArgumentException() {
		assertEquals(PainSeverityLevel.WELL_CONTROLLED, PainSeverityLevel.findSeverityLevelByCode(null));
	}
}
