package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;


public class PatientMedicationPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long patientId;
	private long medicationId;
	
	public PatientMedicationPK() {
	}
	
	public PatientMedicationPK(long patientId, long medicationId) {
		this.patientId = patientId;
		this.medicationId = medicationId;
	}


	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (medicationId ^ (medicationId >>> 32));
		result = prime * result + (int) (patientId ^ (patientId >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PatientMedicationPK other = (PatientMedicationPK) obj;
		if (medicationId != other.medicationId)
			return false;
		if (patientId != other.patientId)
			return false;
		return true;
	}
	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public long getMedicationId() {
		return medicationId;
	}
	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}

}
