package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="checkin_questions", schema="APP")
@JsonIgnoreProperties(ignoreUnknown=true)
public class CheckinQuestion implements Serializable, Identifiable<Long>, Nameable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="checkin_id")
	private long checkinId;
	@Column(name="mouth_throat_pain")
	private String mouthThroatPain;
	@Column(name="does_stop_eat_drink")
	private String doesStopEatDrink;

	public CheckinQuestion() {
	}
	
	public CheckinQuestion(long checkinId, String mouthThroatPain, String doesStopEatDrink) {
		this.checkinId = checkinId;
		this.mouthThroatPain = mouthThroatPain;
		this.doesStopEatDrink = doesStopEatDrink;
	}


	@Override
	public Long getId() {
		return checkinId;
	}

	public long getCheckinId() {
		return checkinId;
	}

	public void setCheckinId(long checkinId) {
		this.checkinId = checkinId;
	}

	public String getMouthThroatPain() {
		return mouthThroatPain;
	}

	public void setMouthThroatPain(String mouthThroatPain) {
		this.mouthThroatPain = mouthThroatPain;
	}

	public String getDoesStopEatDrink() {
		return doesStopEatDrink;
	}

	public void setDoesStopEatDrink(String doesStopEatDrink) {
		this.doesStopEatDrink = doesStopEatDrink;
	}

	@Override
	public String toString() {
		return "CheckinQuestion [checkinId=" + checkinId + ", mouthThroatPain="
				+ mouthThroatPain + ", doesStopEatDrink=" + doesStopEatDrink
				+ "]";
	}

	@Override
	public CharSequence getName() {
		return "Checkin " + checkinId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (checkinId ^ (checkinId >>> 32));
		result = prime
				* result
				+ ((doesStopEatDrink == null) ? 0 : doesStopEatDrink.hashCode());
		result = prime * result
				+ ((mouthThroatPain == null) ? 0 : mouthThroatPain.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinQuestion other = (CheckinQuestion) obj;
		if (checkinId != other.checkinId)
			return false;
		if (doesStopEatDrink == null) {
			if (other.doesStopEatDrink != null)
				return false;
		} else if (!doesStopEatDrink.equals(other.doesStopEatDrink))
			return false;
		if (mouthThroatPain == null) {
			if (other.mouthThroatPain != null)
				return false;
		} else if (!mouthThroatPain.equals(other.mouthThroatPain))
			return false;
		return true;
	}
}
