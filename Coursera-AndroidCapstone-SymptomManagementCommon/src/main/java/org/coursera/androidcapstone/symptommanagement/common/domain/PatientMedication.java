package org.coursera.androidcapstone.symptommanagement.common.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;
import org.coursera.androidcapstone.symptommanagement.common.util.Nameable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="patient_medication", schema="app")
@IdClass(PatientMedicationPK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PatientMedication implements Serializable, Identifiable<PatientMedicationPK>, Nameable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="patient_id")
	private long patientId;
	@Id
	@Column(name="medication_id")
	private long medicationId;
	@Column(name="is_active")
	private char isActive = 'Y';
	
	
	public PatientMedication() {
	}
	
	public PatientMedication(long patientId, long medicationId) {
		this.patientId = patientId;
		this.medicationId = medicationId;
	}

	@Override
	@Transient
	public PatientMedicationPK getId() {
		return new PatientMedicationPK(patientId, medicationId);
	}

	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public long getMedicationId() {
		return medicationId;
	}
	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}
	public char getIsActive() {
		return isActive;
	}
	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "PatientMedication [patientId=" + patientId + ", medicationId="
				+ medicationId + ", isActive=" + isActive + "]";
	}

	@Override
	public CharSequence getName() {
		String active = ( isActive == 'Y' ? "active" : "not active"); 
		return new StringBuilder("Patient ").append(patientId).append(" medication ").append(medicationId).append(" (").append(active).append(")");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + isActive;
		result = prime * result + (int) (medicationId ^ (medicationId >>> 32));
		result = prime * result + (int) (patientId ^ (patientId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PatientMedication other = (PatientMedication) obj;
		if (isActive != other.isActive)
			return false;
		if (medicationId != other.medicationId)
			return false;
		if (patientId != other.patientId)
			return false;
		return true;
	}

}
