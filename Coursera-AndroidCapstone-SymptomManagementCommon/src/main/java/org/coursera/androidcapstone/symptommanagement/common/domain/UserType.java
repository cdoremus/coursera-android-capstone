



package org.coursera.androidcapstone.symptommanagement.common.domain;

import org.coursera.androidcapstone.symptommanagement.common.util.ApplicationUtils;
import org.coursera.androidcapstone.symptommanagement.common.util.Identifiable;

public enum UserType implements Identifiable<String>{
	PATIENT("PA", new Patient()),
	DOCTOR("DR", new Doctor());
	private final String code;
	private final User user;
	
	private UserType(String code, User user) {
		this.code = code;
		this.user = user;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getId() {
		return code;
	}

	public User getUser() {
		return user;
	}
	
	@Override
	public String toString() {
		Character initCap = name().charAt(0);
		String rest = name().substring(1, name().length());
		return new StringBuilder().append(initCap.toString().toUpperCase()).append(rest.toLowerCase()).toString();
	}
	
	public static UserType findUserTypeByCode(String code) {
		return ApplicationUtils.findEnumById(UserType.class, code);
	}

}
