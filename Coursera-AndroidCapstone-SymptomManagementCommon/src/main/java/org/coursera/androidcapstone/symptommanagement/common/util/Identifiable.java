package org.coursera.androidcapstone.symptommanagement.common.util;

public interface Identifiable<K> {

	public K getId();
}
