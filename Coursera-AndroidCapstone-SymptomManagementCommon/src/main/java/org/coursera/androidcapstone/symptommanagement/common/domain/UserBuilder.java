package org.coursera.androidcapstone.symptommanagement.common.domain;


import java.sql.Date;

public class UserBuilder {

	private long userId;
	private String loginName;
	private String phoneNumber;
	private String emailAddress;
	private String lastName;
	private String firstName;
	private String loginPassword;

	private  String userTypeCode;
	private Date dateOfBirth;
	
	private UserBuilder(long userId, String userTypeCode) {
		this.userId = userId;
		this.userTypeCode = userTypeCode;
	}
	
	public static UserBuilder newBuilder(long userId, String userTypeCode) { 
		return new UserBuilder(userId, userTypeCode); 
	}

	public UserBuilder loginName(String value) {
		this.loginName = value;
		return this;
	}

	public UserBuilder phoneNumber(String value) {
		this.phoneNumber = value;
		return this;
	}

	public UserBuilder emailAddress(String value) {
		this.emailAddress = value;
		return this;
	}

	public UserBuilder lastName(String value) {
		this.lastName = value;
		return this;
	}

	public UserBuilder firstName(String value) {
		this.firstName = value;
		return this;
	}

	public UserBuilder loginPassword(String value) {
		this.loginPassword = value;
		return this;
	}
	
	public UserBuilder dateOfBirth(Date value) {
		this.dateOfBirth = value;
		return this;
	}
	
	public User build() {
		UserType userType = UserType.findUserTypeByCode(userTypeCode);
		if (userType == UserType.PATIENT) {
			Patient result = new Patient();
			result.setUserId(userId);
			result.setLoginName(loginName);
			result.setPhoneNumber(phoneNumber);
			result.setEmailAddress(emailAddress);
			result.setLastName(lastName);
			result.setFirstName(firstName);
			result.setLoginPassword(loginPassword);
			result.setUserTypeCode(userTypeCode);
			result.setBirthDate(dateOfBirth);
			return result;
		} else if (userType == UserType.DOCTOR) {
			Doctor result = new Doctor();
			result.setUserId(userId);
			result.setLoginName(loginName);
			result.setPhoneNumber(phoneNumber);
			result.setEmailAddress(emailAddress);
			result.setLastName(lastName);
			result.setFirstName(firstName);
			result.setLoginPassword(loginPassword); 
			result.setUserTypeCode(userTypeCode);
			return result;			
		} else {
			throw new IllegalStateException("User must be a Doctor or a Patient");
		}
		
	}

	public static UserBuilder buildUpon(User original) {
		UserBuilder builder = newBuilder(original.getUserId(), original.getUserTypeCode());
		builder.loginName(original.getLoginName());
		builder.phoneNumber(original.getPhoneNumber());
		builder.emailAddress(original.getEmailAddress());
		builder.lastName(original.getLastName());
		builder.firstName(original.getFirstName());
		builder.loginPassword(original.getLoginPassword());
		builder.dateOfBirth(original.getBirthDate());
		return builder;
	}
}