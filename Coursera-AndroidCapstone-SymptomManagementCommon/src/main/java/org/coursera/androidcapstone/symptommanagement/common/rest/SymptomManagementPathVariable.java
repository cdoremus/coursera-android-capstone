package org.coursera.androidcapstone.symptommanagement.common.rest;

public enum SymptomManagementPathVariable {

	USER_ID(SymptomManagementPathVariable.Name.USER_ID),
	RELATION_ID(SymptomManagementPathVariable.Name.RELATION_ID),
	CHECKIN_ID(SymptomManagementPathVariable.Name.CHECKIN_ID),
	PATIENT_ID(SymptomManagementPathVariable.Name.PATIENT_ID),
	DOCTOR_ID(SymptomManagementPathVariable.Name.DOCTOR_ID),
	MEDICATION_ID(SymptomManagementPathVariable.Name.MEDICATION_ID),
	ALERT_ID(SymptomManagementPathVariable.Name.ALERT_ID),
	USER_FULLNAME(SymptomManagementPathVariable.Name.USER_FULLNAME)
	;
	private final String variable;
	
	private SymptomManagementPathVariable(String variable) {
		this.variable = variable;
	}
	
	public String getVariable() {
		return variable;
	}
	
	@Override
	public String toString() {
		return variable;
	}
	
	/**
	 * Constants used for variable names to allow them to be used in annotation expressions.
	 *
	 */
	public static class Name {
		public static final String USER_ID = "userId";
		public static final String LOGIN_NAME = "loginName";
		public static final String LOGIN_PWD = "loginPassword";
		public static final String RELATION_ID = "relationId";
		public static final String CHECKIN_ID = "checkinId";
		public static final String PATIENT_ID = "patientId";
		public static final String DOCTOR_ID = "doctorId";
		public static final String MEDICATION_ID = "medicationId";
		public static final String ALERT_ID = "alertId";
		public static final String USER_FULLNAME = "userFullname";
	}
	
	/**
	 * Holds expressions for REST path variables paths used in request mappings. 
	 */
	public static class RestPath {
		public static final String USER_ID = "/{" + Name.USER_ID + "}";
		public static final String LOGIN_NAME = "/{" + Name.LOGIN_NAME + "}";
		public static final String LOGIN_PWD = "/{" + Name.LOGIN_PWD + "}";
		public static final String RELATION_ID = "/{" + Name.RELATION_ID + "}";
		public static final String CHECKIN_ID = "/{" + Name.CHECKIN_ID + "}";
		public static final String PATIENT_ID = "/{" + Name.PATIENT_ID + "}";
		public static final String PATIENT_IDS = "/[{" + Name.PATIENT_ID + "}]";//comma-separated list representing an array
		public static final String DOCTOR_ID = "/{" + Name.DOCTOR_ID + "}";
		public static final String MEDICATION_ID = "/{" + Name.MEDICATION_ID + "}";
		public static final String ALERT_ID = "/{" + Name.ALERT_ID + "}";
		public static final String USER_FULLNAME = "/{" + Name.USER_FULLNAME + "}";
	}

}
