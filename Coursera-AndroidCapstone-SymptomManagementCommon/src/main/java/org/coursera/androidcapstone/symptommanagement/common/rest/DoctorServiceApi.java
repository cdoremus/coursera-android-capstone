package org.coursera.androidcapstone.symptommanagement.common.rest;

import java.util.Collection;

import org.coursera.androidcapstone.symptommanagement.common.domain.DoctorAlarm;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedication;

import retrofit.http.Field;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface DoctorServiceApi {

	/**
	 * Find all doctor's patients.
	 *   
	 * @param doctorId The userId of the doctor
	 * @return
	 */
	@GET(RestConstants.DOCTOR_FIND_PATIENTS_REST_PATH)
	public abstract Collection<Patient> findAllPatients(@Path(SymptomManagementPathVariable.Name.DOCTOR_ID) long doctorId);

	/**
	 * Finds a patient's checkin by checkinId
	 * 
	 * @param checkinId The checkinId primary key to the checkin record.
	 * @return
	 */
	@GET(RestConstants.DOCTOR_FIND_CHECKIN_REST_PATH)
	public abstract PatientCheckin findCheckin(@Path(SymptomManagementPathVariable.Name.CHECKIN_ID) long checkinId);

	/**
	 * Find the checkins for a patient.
	 * 
	 * @param patientId The userId of the patient
	 * @return
	 */
	@GET(RestConstants.DOCTOR_FIND_CHECKINS_REST_PATH)
	public abstract Collection<PatientCheckin> findCheckins(@Path(SymptomManagementPathVariable.Name.PATIENT_ID) long patientId);


	/**
	 * Used to assign a patient's medication. The is_active flag is set to 'Y'
	 *  
	 * @param patientId The userId of the patient
	 * @param medicationId The medication identifier 
	 */
	@POST(RestConstants.DOCTOR_ASSIGN_MEDICATION_REST_PATH)
	public abstract PatientMedication assignMedication(@Field(SymptomManagementPathVariable.Name.PATIENT_ID) long patientId, @Field(SymptomManagementPathVariable.Name.MEDICATION_ID) long medicationId);

	
	/**
	 * Used to remove a patient's medication by setting the is_active flag to 'N'.
	 *  
	 * @param patientId The userId of the patient
	 * @param medicationId The medication identifier 
	 */
	@POST(RestConstants.DOCTOR_REMOVE_ASSIGNED_MEDICATION_REST_PATH)
	public abstract PatientMedication removeAssignedMedication(@Field(SymptomManagementPathVariable.Name.PATIENT_ID) long patientId, @Field(SymptomManagementPathVariable.Name.MEDICATION_ID) long medicationId);

	
	/**
	 * Finds all patient alarms for a doctor.
	 * 
	 * @param doctorId The userId of the doctor
	 * @return
	 */
	@GET(RestConstants.DOCTOR_CHECK_ALL_PATIENTS_ALARMS_REST_PATH)
	public abstract Collection<DoctorAlarm> findAlarms(@Path(SymptomManagementPathVariable.Name.DOCTOR_ID) long doctorId);

}