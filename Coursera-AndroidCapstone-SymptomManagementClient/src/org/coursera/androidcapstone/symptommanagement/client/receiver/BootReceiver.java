package org.coursera.androidcapstone.symptommanagement.client.receiver;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * 
 * {@link BroadcastReceiver} run a boot time used to start other receivers that do notifications.
 *
 */
public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		// Create an Intent to broadcast to the CheckinNotificationReceiver
		Intent checkinNotificationReceiverIntent = new Intent(context, CheckinNotificationReceiver.class);
		//Create a pending intent to use with an alarm manager
		context.sendBroadcast(checkinNotificationReceiverIntent);
		
		
		
		// Create an Intent to broadcast to the AlarmNotificationReceiver
		Intent alarmNotificationReceiverIntent = new Intent(context, DoctorAlertNotificationReceiver.class);
		//Create a pending intent to use with an alarm manager
		PendingIntent alarmNotificationReceiverPendingIntent = PendingIntent.getBroadcast(context, 0, alarmNotificationReceiverIntent, PendingIntent.FLAG_ONE_SHOT);
		//set periodic alarm to invoke
		alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime() + ClientConstants.DOCTOR_ALARM_CHECK_PERIOD_MINUTES, AlarmManager.INTERVAL_HALF_HOUR,
				alarmNotificationReceiverPendingIntent);
		
	}

}
