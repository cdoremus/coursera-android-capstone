package org.coursera.androidcapstone.symptommanagement.client.constant;


/**
 * 
 * Identifies object as having a key parameter value as used for intent extras and preferences.
 * 
 */
public interface KeyParameter {

	public String getKey();
}
