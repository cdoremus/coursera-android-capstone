package org.coursera.androidcapstone.symptommanagement.client.constant;

import org.coursera.androidcapstone.symptommanagement.common.constant.CommonConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.content.Intent;
import android.net.Uri;

public class ClientConstants {

	public static final String ANDROID_PACKAGE_NAME = "org.coursera.androidcapstone.symptommanagementclient";
	
	//Cannot be localhost in Android emulator
	public static final String REST_HOST_URL = "http://192.168.2.5:8080"; //http://10.0.2.2
	

	// Notification Sound
	public static final Uri NOTIFICATION_SOUND_URI = Uri
			.parse("android.resource://" + ANDROID_PACKAGE_NAME
					+ R.raw.theme_from_st_elsewhere);
	
	public static final String[] DEFAULT_CHECKIN_NOTIFICATION_TIMES = {
			"08:00",
			"12:00",
			"16:00",
			"20:00"
			};
	
	
	
	public static final long DOCTOR_ALARM_CHECK_PERIOD_MINUTES = 30 * CommonConstants.MILIS_PER_MINUTE;//30 minutes
	
	
	
	/**
	 * Keys used for {@link Intent} extras.
	 */
	public enum IntentExtraKey implements KeyParameter {
		PATIENT,
		PATIENT_MEDICATIONS,
		CHECKIN,
		MEDICATION_QUESTION,
		DOCTOR_ID,
		MEDICATION,
		ALARMS,
		ACTION,
		;
		
		public String getKey() {
			return name();
		}
		
	}
	
	
	public enum Action {
		NEW,
		CHANGE,
		SAVE,
		DELETE,
		CANCEL
		;
	}
	
	
	public enum UserPreferenceKey implements KeyParameter {
		USER_ID("userId"),
		USER_FULLNAME("userFullname"),
		USER_TYPE_CODE("userTypeCode")
		;
		private final String key;
		
		private UserPreferenceKey(String key) {
			this.key = key;
		}
		
		public String getKey() {
			return key;
		}
		
		@Override
		public String toString() {
			return key;
		}
	}
	
	
	
	public enum ClientNotificationTimePreferenceKey implements KeyParameter {
		NOTIFICATION_TIME1("notificationTime1"),
		NOTIFICATION_TIME2("notificationTime2"),
		NOTIFICATION_TIME3("notificationTime3"),
		NOTIFICATION_TIME4("notificationTime4")
		;
		private final String key;
		
		private ClientNotificationTimePreferenceKey(String key) {
			this.key = key;
		}
		
		public String getKey() {
			return key;
		}
		
		@Override
		public String toString() {
			return key;
		}
	}
}
