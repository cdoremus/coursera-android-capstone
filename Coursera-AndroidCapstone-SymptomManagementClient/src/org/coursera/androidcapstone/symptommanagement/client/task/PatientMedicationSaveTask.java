package org.coursera.androidcapstone.symptommanagement.client.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.coursera.androidcapstone.symptommanagement.client.activity.MedicationUpdateSelectionActivity;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.util.RestTemplateManager;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedication;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;


/**
 * For create, update and logically delete a patient medication.
 * 
 *
 */
public class PatientMedicationSaveTask extends AsyncTask<Long, Void, PatientMedication[]> {

	private static final String TAG = PatientMedicationSaveTask.class.getSimpleName();
	private Patient patient;
	private Medication[] newMedications;
	private Medication[] oldMedications;
	private Activity activity;
	
	
	public PatientMedicationSaveTask(Activity activity, Patient patient, 
			Medication[] oldMedications, Medication[] newMedications) {
		this.activity = activity;
		this.patient = patient;
		this.oldMedications = oldMedications;
		this.newMedications = newMedications;
	}
	
	
	
	@Override
	protected void onPostExecute(PatientMedication[] result) {
		
		//start next activity refreshing 
		Intent intent = new Intent(activity, MedicationUpdateSelectionActivity.class);
		intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
		activity.startActivity(intent);
//		Toast.makeText(context, "Medication " + medicationId + " "  + action.name().toLowerCase() +"ed for patient "  + patientId, Toast.LENGTH_LONG).show();
		
	}




	@Override
	protected PatientMedication[] doInBackground(Long... params) {
		long patientId = 0L;		
		if (params.length == 1) {
			patientId = params[0];
		}
		
        RestTemplateManager<PatientMedication> restTemplate = new RestTemplateManager<PatientMedication>(PatientMedication.class);
		//Logically Delete oldMedications that are not in newMedicationsList
        final String deleteMedUrl = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_REMOVE_ASSIGNED_MEDICATION_REST_PATH;
        Log.i(TAG,"Delete med URL: " + deleteMedUrl);
        Log.i(TAG, "Old Medications: " + Arrays.toString(oldMedications));
        Log.i(TAG, "New Medications: " + Arrays.toString(newMedications));
        for (Medication oldMedication : oldMedications) {
        	long oldMedicationId = oldMedication.getMedicationId();
//            for (Medication newMedication : newMedications) {
//            	if (oldMedicationId == newMedication.getMedicationId()) {
                	PatientMedication deleteMed = new PatientMedication(patientId, oldMedicationId);            	
                	PatientMedication deletedPatientMedication = restTemplate.invokePost(deleteMed, deleteMedUrl);
                	Log.i(TAG, "Deleted patient medication: " + deletedPatientMedication);
            		break;
//            	}
//            }
		}
        

        
		List<PatientMedication> medList = new ArrayList<PatientMedication>();
		//Post new patient medications to server 		
        final String assignMedUrl = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_ASSIGN_MEDICATION_REST_PATH;
        Log.i(TAG,"Assign med URL: " + assignMedUrl);
        for (Medication medication : newMedications) {
        	PatientMedication patientMedication = new PatientMedication(patientId, medication.getMedicationId());
        	PatientMedication postedPatientMedication = restTemplate.invokePost(patientMedication, assignMedUrl);
        	Log.i(TAG, "Added patient medication: " + postedPatientMedication);
			medList.add(postedPatientMedication);
		}
        return medList.toArray(new PatientMedication[]{});
	}

}
