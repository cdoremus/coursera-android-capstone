package org.coursera.androidcapstone.symptommanagement.client.task;

import java.util.Arrays;
import java.util.Collection;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.data.PatientListAdapter;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

public class DoctorPatientsTask extends AsyncTask<Long, Void, Collection<Patient>> {
	public static final String TAG = DoctorPatientsTask.class.getSimpleName(); 
	
	private Context context;
	private ListView listView;
	
	
	public DoctorPatientsTask(Context context, ListView listView) {
		this.context = context;
		this.listView = listView;
	}
	
	
	@Override
	protected void onPostExecute(Collection<Patient> result) {
		Log.i(TAG, "Patients found: " + result);
		PatientListAdapter adapter = new PatientListAdapter(context, R.layout.patient_list_item, result.toArray(new Patient[]{}));
		listView.setAdapter(adapter);
	}

	@Override
	protected Collection<Patient> doInBackground(Long... params) {
        final String url = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_PATIENTS_ENDPOINT_PATH + "/" + params[0];
        Log.i(TAG, "REST url: " + url);
        RestTemplate restTemplate = new RestTemplate(); 
        Patient[] patients = restTemplate.getForObject(url, Patient[].class);	        
		return Arrays.asList(patients);
	}

}
