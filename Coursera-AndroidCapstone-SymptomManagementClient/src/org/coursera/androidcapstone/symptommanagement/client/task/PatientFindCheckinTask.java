package org.coursera.androidcapstone.symptommanagement.client.task;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.data.CheckinListAdapter;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

public class PatientFindCheckinTask extends AsyncTask<Long, Void, PatientCheckin> {

	private static final String TAG = PatientFindCheckinTask.class.getSimpleName();
	
	private Context context;
	private ListView listView;
	
	
	public PatientFindCheckinTask(Context context, ListView listView) {
		this.context = context;
		this.listView = listView;
	}

	@Override
	protected void onPostExecute(PatientCheckin result) {
		Log.i(TAG, "Checkin found: " + result);
//		CheckinListAdapter adapter = new CheckinListAdapter(context, R.layout.checkin_list_item, result);
//		listView.setAdapter(adapter);
	}
	
	@Override
	protected PatientCheckin doInBackground(Long... params) {
        final String url = ClientConstants.REST_HOST_URL + RestConstants.DOCTOR_CHECKIN_ENDPOINT_PATH + "/" + params[0];
        Log.i(TAG, "Find checkin REST url: " + url);
        RestTemplate restTemplate = new RestTemplate(); 
        PatientCheckin checkin = restTemplate.getForObject(url, PatientCheckin.class);	        
		
		return checkin;
	}
}
