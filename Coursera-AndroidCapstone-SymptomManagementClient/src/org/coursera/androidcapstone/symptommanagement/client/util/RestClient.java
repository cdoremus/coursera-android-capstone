package org.coursera.androidcapstone.symptommanagement.client.util;

public interface RestClient<T> {

	public abstract T invokeGetReturnOne(String restUrl);

	public abstract T[] invokeGetReturnMany(String restUrl);

	public abstract T invokePost(T entity, String restUrl);

}