package org.coursera.androidcapstone.symptommanagement.client.data;

import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MedicationQuestionListAdapter extends ArrayAdapter<MedicationQuestion> {
	private static final String TAG = MedicationQuestionListAdapter.class.getSimpleName();
	
	private MedicationQuestion[] medQuestions;
	private Context context;
	private int layoutResourceId;

	public MedicationQuestionListAdapter(Context context, int layoutResourceId, MedicationQuestion[] medQuestions) {
		super(context, layoutResourceId, medQuestions);
		this.context = context;
		this.medQuestions = medQuestions;
		this.layoutResourceId = layoutResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View rowView = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			rowView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.medQuestionIdText = (TextView)rowView.findViewById(R.id.medQuestionIdText);
			holder.medNameText = (TextView)rowView.findViewById(R.id.medNameText);
			holder.medQuestionHasTakenText = (TextView)rowView.findViewById(R.id.medQuestionHasTakenText);
			holder.medQuestionTakenDateTimeText = (TextView)rowView.findViewById(R.id.medQuestionTakenDateTimeText);
		} else {
			holder = (ViewHolder)rowView.getTag();
		}
		
		if (medQuestions.length != 0) {
			MedicationQuestion medQuestion = medQuestions[position];
			Log.i(TAG, "Question found for position " + position + ": " + medQuestion);
			holder.medQuestionIdText.setText(Long.toString(medQuestion.getMedicationId()));
			Medication medication = medQuestion.getMedication();
			holder.medNameText.setText(medication != null ? medication.getMedicationName() : "");
			holder.medQuestionHasTakenText.setText(medQuestion.getHasTakenMedication() == 'Y' ? "Yes" : "No");
			holder.medQuestionTakenDateTimeText.setText(medQuestion.getDateTimeMedicineTaken() == null ? "" : medQuestion.getDateTimeMedicineTaken().toString());
		}
		return rowView;
	}

	static class ViewHolder {
		TextView medQuestionIdText;
		TextView medNameText;
		TextView medQuestionHasTakenText;
		TextView medQuestionTakenDateTimeText;
	}

}
