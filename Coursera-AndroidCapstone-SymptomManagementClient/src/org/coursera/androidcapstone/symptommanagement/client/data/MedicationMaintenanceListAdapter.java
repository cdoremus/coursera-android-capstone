package org.coursera.androidcapstone.symptommanagement.client.data;

import org.coursera.androidcapstone.symptommanagement.client.activity.MedicationUpdateSelectionActivity;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.Action;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientMedication;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MedicationMaintenanceListAdapter extends ArrayAdapter<Medication> implements OnClickListener {

	private static final String TAG = MedicationMaintenanceListAdapter.class.getSimpleName();
	
	private Medication[] medications;
	private Context context;
	private int layoutResourceId;
	private ListView listView; 
	private Patient patient;
	
	public MedicationMaintenanceListAdapter(Context context, int layoutResourceId, 
			Medication[] medications, ListView listView, Patient patient) {
		super(context, layoutResourceId, medications);
		this.context = context;
		this.medications = medications;
		this.layoutResourceId = layoutResourceId;
		this.listView = listView;
		this.patient = patient;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View rowView = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
			rowView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.medNameText = (TextView)rowView.findViewById(R.id.updateMedicationNameText);
			holder.editPatientMedicationsButton = (Button)rowView.findViewById(R.id.editPatientMedicationsButton);
			holder.editPatientMedicationsButton.setOnClickListener(this);
			holder.deletePatientMedicationsButton = (Button)rowView.findViewById(R.id.deletePatientMedicationsButton);
			holder.deletePatientMedicationsButton.setOnClickListener(this);
		} else {
			holder = (ViewHolder)rowView.getTag();
		}
		
		if (medications.length != 0) {
			Medication medication = medications[position];
			holder.medNameText.setText(medication != null ? medication.getMedicationName() : "");
		}
		return rowView;
	}

	static class ViewHolder {
		TextView medNameText;
		Button editPatientMedicationsButton;
		Button deletePatientMedicationsButton;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		int position = listView.getPositionForView(v);
		Intent intent = new Intent(context, MedicationUpdateSelectionActivity.class);
		switch (id) {
			//change med in row
			case R.id.editPatientMedicationsButton:
				Log.i(TAG, "Change button clicked for position: " + position);
				intent.putExtra(IntentExtraKey.MEDICATION.getKey(), medications[position]);
				intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
				intent.putExtra(IntentExtraKey.ACTION.getKey(), Action.CHANGE);
				context.startActivity(intent);			
				
				break;	
			//delete med in row	
			case R.id.deletePatientMedicationsButton:
				//FIXME: Should just 
				Log.i(TAG, "Delete button clicked for position: " + position);
				intent.putExtra(IntentExtraKey.MEDICATION.getKey(), medications[position]);
				intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
				intent.putExtra(IntentExtraKey.ACTION.getKey(), Action.DELETE);
				context.startActivity(intent);			
				
				break;
		default:
			break;
	}
	
		
	}

}
