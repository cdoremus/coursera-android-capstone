package org.coursera.androidcapstone.symptommanagement.client.data;

import java.sql.Timestamp;
import java.util.Calendar;

import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

public class PatientMedicationsListAdapter extends
		ArrayAdapter<MedicationQuestion> implements OnClickListener {

	private static final String TAG = PatientMedicationsListAdapter.class.getSimpleName();
	
	private MedicationQuestion[] medications;
	private Context context;
	private int layoutResourceId;
	private ListView listView;
	private Calendar dateAndTime = Calendar.getInstance();
	private TextView currentDateTimeText;

	public PatientMedicationsListAdapter(Context context, int layoutResourceId,  ListView listView,	MedicationQuestion[] medications) {
		super(context, layoutResourceId, medications);
		this.context = context;
		this.medications = medications;
		this.listView = listView;
		this.layoutResourceId = layoutResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View rowView = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			rowView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.medicationIdText = (TextView) rowView
					.findViewById(R.id.medicationItemIdText);
			holder.medicationNameText = (TextView) rowView
					.findViewById(R.id.medicationItemNameText);
			holder.medicationTakenCheckbox = (CheckBox) rowView
					.findViewById(R.id.medicationTakenCheckbox);
			holder.medicationTakenCheckbox.setOnClickListener(this);
			holder.medicationItemDateTimeTakenText = (TextView) rowView
					.findViewById(R.id.medicationItemDateTimeTakenText);

		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		if (medications.length != 0) {
			MedicationQuestion medication = medications[position];
			holder.medicationIdText.setText(Long.toString(medication
					.getMedicationId()));
			holder.medicationNameText.setText(medication.getMedication()
					.getMedicationName());
			holder.medicationTakenCheckbox.setSelected(medication
					.getHasTakenMedication() == 'Y' ? true : false);
			Timestamp dateTaken = medication.getDateTimeMedicineTaken();
			holder.medicationItemDateTimeTakenText
					.setText(dateTaken == null ? "" : dateTaken.toString());
		}
		return rowView;
	}

	static class ViewHolder {
		TextView medicationIdText;
		TextView medicationNameText;
		CheckBox medicationTakenCheckbox;
		TextView medicationItemDateTimeTakenText;
	}

	@Override
	public void onClick(View v) {
		Log.i(TAG, "Checkbox clicked");
		int id = v.getId();
		int position = listView.getPositionForView(v);
		//get the list element at the position you want
		View rowView = listView.getChildAt(position);
		//get the Change button in that row and click on it
		CheckBox currentCheckbox = (CheckBox)rowView.findViewById(R.id.medicationTakenCheckbox);
		currentDateTimeText = (TextView)rowView.findViewById(R.id.medicationItemDateTimeTakenText);
		if (id == R.id.medicationTakenCheckbox) {
			if (currentCheckbox.isChecked()) {
				// pick date and time
				chooseDate(v);
				chooseTime(v);
				//set text view
			} else {//unchecked
				//remove any date/time value that may be there
				currentDateTimeText.setText("");
			}
		}

	}

	private void updateDateAndTimeTextView(TextView currentDateTimeText) {
		currentDateTimeText.setText(DateUtils.formatDateTime(
				context, dateAndTime.getTimeInMillis(),
				DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));
	}

	public void chooseDate(View v) {
		new DatePickerDialog(context, dateListener, dateAndTime.get(Calendar.YEAR),
				dateAndTime.get(Calendar.MONTH),
				dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
	}

	public void chooseTime(View v) {
		new TimePickerDialog(context, timeListener, dateAndTime.get(Calendar.HOUR_OF_DAY),
				dateAndTime.get(Calendar.MINUTE), true).show();
	}

	DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			dateAndTime.set(Calendar.YEAR, year);
			dateAndTime.set(Calendar.MONTH, monthOfYear);
			dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateDateAndTimeTextView(currentDateTimeText);
	}
	};

	TimePickerDialog.OnTimeSetListener timeListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateAndTime.set(Calendar.MINUTE, minute);
			updateDateAndTimeTextView(currentDateTimeText);
		}
	};

}
