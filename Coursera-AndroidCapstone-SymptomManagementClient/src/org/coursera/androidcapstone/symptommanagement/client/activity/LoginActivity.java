package org.coursera.androidcapstone.symptommanagement.client.activity;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.UserPreferenceKey;
import org.coursera.androidcapstone.symptommanagement.common.domain.User;
import org.coursera.androidcapstone.symptommanagement.common.domain.UserType;
import org.coursera.androidcapstone.symptommanagement.common.rest.RestConstants;
import org.coursera.androidcapstone.symptommanagementclient.R;
import org.springframework.web.client.RestTemplate;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity implements OnClickListener {
	
	public static final String TAG = LoginActivity.class.getSimpleName();
	
	public String host = RestConstants.HOST_URL;
	private EditText usernameTextView;
	private EditText passwordTextView;
	private Button loginButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		usernameTextView = (EditText)findViewById(R.id.user_name);
		Log.i(TAG, "Get reference to username field: " + usernameTextView);
		passwordTextView = (EditText)findViewById(R.id.user_password);
		Log.i(TAG, "Get reference to password field: " + passwordTextView);
		loginButton = (Button)findViewById(R.id.sign_in_button);
		loginButton.setOnClickListener(this);
}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
			case R.id.action_home:
				startActivity(new Intent(this, HomeScreenActivity.class));				
				break;
			case R.id.action_login:
				// Launch LoginActivity
				startActivity(new Intent(this, LoginActivity.class));
				break;
			default:
				break;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		//get username and password
		String user = usernameTextView.getText().toString();
		Log.i(TAG, "Username: " + user);
		String pass = passwordTextView.getText().toString();
		Log.i(TAG, "Password: " + pass);
		
		//Call server service to verify user
		new LoginRestTask().execute(user,pass);
		
	}
	
	
	private class LoginRestTask extends AsyncTask<String, Void, User> {
	
		@Override
		protected void onPostExecute(User result) {

			if (result != null) {
				//Update shared user preferences
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);				
				Editor editor = prefs.edit();
				editor.putLong(UserPreferenceKey.USER_ID.getKey(), result.getUserId());
				editor.putString(UserPreferenceKey.USER_FULLNAME.getKey(), result.getFullName());
				editor.putString(UserPreferenceKey.USER_TYPE_CODE.getKey(), result.getUserTypeCode());
				editor.apply();
				
				//start appropriate activity for user type
				UserType userType = result.getUserType();
				if (userType == UserType.PATIENT) {
					startActivity(new Intent(LoginActivity.this, CheckinActivity.class));
				} else if (userType == UserType.DOCTOR ){
					startActivity(new Intent(LoginActivity.this, PatientListActivity.class));			
				} else {
					Toast.makeText(LoginActivity.this, "Login credentials verified, but user is not a Patient or Doctor type.", Toast.LENGTH_LONG).show();					
				}
			} else {
				Toast.makeText(LoginActivity.this, "Login credentials were not verified. Please try again.", Toast.LENGTH_LONG).show();
			}
		}
	

		@Override
		protected User doInBackground(String... params) {
	        final String url = ClientConstants.REST_HOST_URL + RestConstants.USER_FIND_BY_CREDENTIALS_ENDPOINT_PATH + "/" + params[0] + "/" + params[1];
	        Log.i(TAG, "Rest URL: " + url);
	        RestTemplate restTemplate = new RestTemplate(); 
	        User user = restTemplate.getForObject(url, User.class);	        
			return user;
		}
		
	}		
		


}