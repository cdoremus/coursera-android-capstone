package org.coursera.androidcapstone.symptommanagement.client.activity;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.Action;
import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.task.PatientMedicationMaintenanceTask;
import org.coursera.androidcapstone.symptommanagement.client.task.PatientMedicationSaveTask;
import org.coursera.androidcapstone.symptommanagement.common.domain.Medication;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class PatientMedicationMaintenanceFragment extends Fragment implements OnItemClickListener, OnClickListener {

	private static final String TAG = PatientMedicationMaintenanceFragment.class.getSimpleName();
	
	private Patient patient;
//	private Medication[] medications;
	private Medication medication;
	
	public PatientMedicationMaintenanceFragment() {	
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle extras = getActivity().getIntent().getExtras();
		if (extras != null) { 
			patient = (Patient)extras.get(IntentExtraKey.PATIENT.getKey());
		}
		
				
		View rootView = inflater.inflate(R.layout.fragment_patient_medication_maintenance, container,
				false);
		
		ListView listView = (ListView)rootView.findViewById(R.id.updateMedicationlistView);
		listView.setOnItemClickListener(this);
		
		new PatientMedicationMaintenanceTask(getActivity(), listView, patient).execute(patient.getUserId());
		
		
		Button newPatientMedicationButton = (Button)rootView.findViewById(R.id.newPatientMedicationButton);
		newPatientMedicationButton.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		Intent intent;
		switch (id) {
//			//save meds button
//			case R.id.savePatientMedicationsButton:
//				Log.i(TAG, "Save Meds button clicked");
//				//FIXME: Save patient meds
//				medications
//				new PatientMedicationSaveTask(getActivity(), patient, Action.SAVE, 
//						oldMedication, newMedication).execute(patient.getUserId(), checkin.getCheckinId());
//				Toast.makeText(getActivity(), "Medications updated for patient " + patient.getFullName(), Toast.LENGTH_LONG).show();
//				break;
			//new med button	
			case R.id.newPatientMedicationButton:
				Log.i(TAG, "New Med button clicked");
				intent = new Intent(getActivity(), MedicationUpdateSelectionActivity.class);
				intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
				intent.putExtra(IntentExtraKey.ACTION.getKey(), Action.NEW);
				startActivity(intent);			
				
				break;
			default:
				break;
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
//			medication = medications[position];
//			Log.i(TAG, "Medications selected: " + medication);
			
//			Toast.makeText(getActivity(), "Med selected: " + medication, Toast.LENGTH_LONG).show();
	}
}
