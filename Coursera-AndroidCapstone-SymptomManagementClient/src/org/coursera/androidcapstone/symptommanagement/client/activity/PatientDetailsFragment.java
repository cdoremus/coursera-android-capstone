package org.coursera.androidcapstone.symptommanagement.client.activity;

import java.util.List;

import org.coursera.androidcapstone.symptommanagement.client.constant.ClientConstants.IntentExtraKey;
import org.coursera.androidcapstone.symptommanagement.client.data.MedicationQuestionListAdapter;
import org.coursera.androidcapstone.symptommanagement.common.domain.MedicationQuestion;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainSeverityLevel;
import org.coursera.androidcapstone.symptommanagement.common.domain.PainStopEatDrinkQuestionResponse;
import org.coursera.androidcapstone.symptommanagement.common.domain.Patient;
import org.coursera.androidcapstone.symptommanagement.common.domain.PatientCheckin;
import org.coursera.androidcapstone.symptommanagementclient.R;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class PatientDetailsFragment extends Fragment implements OnItemClickListener, OnClickListener {
	
	public static final String TAG = PatientDetailsFragment.class.getSimpleName();
	
	private MedicationQuestion[] medicationQuestions;
	private Patient patient;
	private PatientCheckin checkin;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Bundle extras = getActivity().getIntent().getExtras();
		patient = (Patient)extras.get(IntentExtraKey.PATIENT.getKey());
		checkin = (PatientCheckin)extras.get(IntentExtraKey.CHECKIN.getKey());
		Log.i(TAG, "Checkin extra: " + checkin);
		
		long checkinId = checkin.getCheckinId();	
		
		View rootView = inflater.inflate(R.layout.fragment_patient_details, container,
				false);
		//Add patientId, full name, and date of birth to UI		
		TextView patientName = (TextView)rootView.findViewById(R.id.patientNameLabel);
		patientName.setText(new StringBuilder("Patient: ").append(patient.getFullName()).append("(ID=").append(patient.getUserId()).append(")"));
		TextView checkinDateTime = (TextView)rootView.findViewById(R.id.checkinDateTimeLabel);
		checkinDateTime.setText(new StringBuilder("Checkin DateTime: ").append(checkin.getCheckinDateTime()));

		TextView mouthPainSeverityAnswer = (TextView)rootView.findViewById(R.id.mouthPainSeverityAnswer);
		mouthPainSeverityAnswer.setText(PainSeverityLevel.findSeverityLevelByCode(checkin.getMouthThroatPain()).getDisplayName());
		TextView stopEatDrinkAnswer = (TextView)rootView.findViewById(R.id.stopEatDrinkAnswer);
		stopEatDrinkAnswer.setText(PainStopEatDrinkQuestionResponse.findQuestonResponseByCode(checkin.getDoesStopEatDrink()).getDisplayName());
		TextView screenTitle = (TextView)rootView.findViewById(R.id.patientDetailsLabel);
		screenTitle.setText(new StringBuilder("Checkin ").append(checkinId));

		ListView listView = (ListView)rootView.findViewById(R.id.didYouTakeMedslistView); 
		
		List<MedicationQuestion> questions = checkin.getMedicationQuestions();
		medicationQuestions =  questions.toArray(new MedicationQuestion[]{});
		MedicationQuestionListAdapter adapter = new MedicationQuestionListAdapter(getActivity(), R.layout.med_question_item, medicationQuestions);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(this);
		
		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
//		MedicationQuestion selectedQuestion = medicationQuestions[position];
//		
//		Intent intent = new Intent(getActivity(), PatientMedicationMaintenanceActivity.class);
//		intent.putExtra(IntentExtraKey.PATIENT.getKey(), patient);
//		intent.putExtra(IntentExtraKey.CHECKIN.getKey(), checkin);
//		intent.putExtra(IntentExtraKey.MEDICATION_QUESTION.getKey(), selectedQuestion);
//		startActivity(intent);			
		
	}

	@Override
	public void onClick(View v) {
		
	}


}
